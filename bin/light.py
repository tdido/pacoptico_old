import smbus
import sys
import threading
import os

pidfile = "/opt/pacoptico/pid"

try:
    with open(pidfile) as fh:
        pid=int(fh.readline())
        os.kill(pid,15)
        os.remove(pidfile)
except IOError:
    pid=None

address = 0x2C
timeout = 30.0

bus = smbus.SMBus(1)

cmd = sys.argv[1]

def on():
    bus.write_byte_data(address,0,0xFF)
    bus.write_byte_data(address,0x80,0xFF)
    with open(pidfile,"w") as ofh:
        ofh.write(str(os.getpid()))

def off():
    bus.write_byte_data(address,0,0x00)
    bus.write_byte_data(address,0x80,0x00)
    try:
        os.remove(pidfile)
    except OSError:
        pass

if cmd == "off":
    off()
elif cmd == "on":
    on()
    turnoff = threading.Timer(timeout, off)
    turnoff.start()
elif cmd == "status":
    print(bus.read_byte_data(address,0))
else:
    raise ValueError("cmd not recognised")
